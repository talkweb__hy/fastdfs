Name: fastdfs
Version: 6.06
Release: 1
Summary: FastDFS server and client
License: GPL3.0
Group: Arch/Tech
URL: 	https://github.com/happyfish100/fastdfs
Source: https://github.com/happyfish100/fastdfs/archive/V%{version}.tar.gz

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n) 

Requires: gcc cmake
BuildRequires: libfastcommon-devel >= 1.0.43

%description
This package provides tracker & storage of fastdfs

%package -n fastdfs-server
Requires: libfastcommon >= 1.0.43
Summary: fastdfs tracker & storage

%package -n fastdfs-tools
Requires: libfastcommon
Summary: fastdfs tools

%package -n libfdfsclient
Requires: libfastcommon
Summary: The client dynamic library of fastdfs

%package -n libfdfsclient-devel
Requires: libfdfsclient
Summary: The client header of fastdfs

%description -n fastdfs-server
This package provides tracker & storage of fastdfs

%description -n libfdfsclient
This package is client dynamic library of fastdfs

%description -n libfdfsclient-devel
This package is client header of fastdfs client

%description -n fastdfs-tools
This package is tools for fastdfs

%prep
%setup -q

%build
./make.sh

%install
rm -rf %{buildroot}
DESTDIR=$RPM_BUILD_ROOT ./make.sh install
rm -f %{buildroot}/etc/fdfs/http.conf.sample
rm -f %{buildroot}/etc/fdfs/mime.types.sample

%post -n fastdfs-server
/sbin/chkconfig --add fdfs_trackerd
/sbin/chkconfig --add fdfs_storaged

%preun -n fastdfs-server
/sbin/chkconfig --del fdfs_trackerd
/sbin/chkconfig --del fdfs_storaged

%postun

%clean
rm -rf %{buildroot}

%files -n fastdfs-server
%defattr(-,root,root,-)
/usr/bin/fdfs_trackerd
/usr/bin/fdfs_storaged
/usr/bin/restart.sh
/usr/bin/stop.sh
/etc/init.d/*
/etc/fdfs/tracker.conf.sample
/etc/fdfs/storage.conf.sample
/etc/fdfs/storage_ids.conf.sample

%files -n libfdfsclient
/usr/lib64/libfdfsclient*
/usr/lib/libfdfsclient*
/etc/fdfs/client.conf.sample

%files -n libfdfsclient-devel
%defattr(-,root,root,-)
/usr/include/fastdfs/*

%files -n fastdfs-tools
/usr/bin/fdfs_monitor
/usr/bin/fdfs_test
/usr/bin/fdfs_test1
/usr/bin/fdfs_crc32
/usr/bin/fdfs_upload_file
/usr/bin/fdfs_download_file
/usr/bin/fdfs_delete_file
/usr/bin/fdfs_file_info
/usr/bin/fdfs_appender_test
/usr/bin/fdfs_appender_test1
/usr/bin/fdfs_append_file
/usr/bin/fdfs_upload_appender
/usr/bin/fdfs_regenerate_filename

%changelog
* Fri Sep 11 2020 huyan90325 <huyan90325@talkweb.com.cn> - 1.0.44-1
- Fastdfs package init
